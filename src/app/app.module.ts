import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
import { StudentDataImplService } from './service/student-data-impl.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { StudentsAddComponent } from './students/students-add/students-add.component';
import { StudentsViewComponent } from './students/students-view/students-view.component';
import { StudentsListComponent } from './students/students-list/students-list.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentRoutingModule } from './students/student-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    MyNavComponent,
    StudentsAddComponent,
    StudentsViewComponent,
    StudentsListComponent,
    FileNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    StudentRoutingModule,
    AppRoutingModule,
  ],
  providers: [
    {provide: StudentService, useClass: StudentDataImplService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
