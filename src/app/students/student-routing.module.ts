import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { StudentsComponent } from './students.component';
import { StudentsAddComponent } from './students-add/students-add.component';
import { StudentsViewComponent } from './students-view/students-view.component';

const studentRoutes: Routes = [
    {
        path: 'view', component: StudentsViewComponent
    },
    {
        path: 'add', component: StudentsAddComponent
    },
    {
        path: 'list', component: StudentsComponent
    },
    {
        path: 'detail/:id', component: StudentsViewComponent
    }
]

@NgModule ({
    imports: [
        RouterModule.forRoot(studentRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class StudentRoutingModule {}