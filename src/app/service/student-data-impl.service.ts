import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
import { Student } from '../entity/student';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class StudentDataImplService extends StudentService {

  constructor(private http: HttpClient) { 
    super();
  }
  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('assets/student.json');
  }

  getStudent(id: number): Observable<Student> {
    return;
  }

}